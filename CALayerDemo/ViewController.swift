//
//  ViewController.swift
//  CALayerDemo
//
//  Created by Admin on 12.02.2020.
//  Copyright © 2020 Denis Silych. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var shapeLayer: CAShapeLayer! {
        didSet {
            shapeLayer.lineWidth = 20
            shapeLayer.lineCap = .round
            shapeLayer.fillColor = nil
            shapeLayer.strokeEnd = 1
            shapeLayer.strokeColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1).cgColor
        }
    }
    var overShapeLayer: CAShapeLayer! {
        didSet {
            overShapeLayer.lineWidth = 20
            overShapeLayer.lineCap = .round
            overShapeLayer.fillColor = nil
            overShapeLayer.strokeEnd = 0
            overShapeLayer.strokeColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).cgColor
        }
    }
    var gradientLayer: CAGradientLayer! {
        didSet {
            gradientLayer.startPoint = CGPoint(x: 0, y: 0)
            gradientLayer.endPoint = CGPoint(x: 0, y: 1)
            let startColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1).cgColor
            let middleColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1).cgColor
            let endColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1).cgColor
            gradientLayer.colors = [startColor, middleColor, endColor]
            gradientLayer.locations = [0, 0.3, 1]
        }
    }
    @IBOutlet weak var image: UIImageView! {
        didSet {
            image.layer.cornerRadius = image.frame.size.height/2
            image.layer.masksToBounds = true
            let borderColor = UIColor.cyan
            image.layer.borderColor = borderColor.cgColor
            image.layer.borderWidth = 10
        }
    }
    @IBOutlet weak var button: UIButton! {
        didSet {
            button.layer.shadowOffset = CGSize(width: 0, height: 5)
            button.layer.shadowOpacity = 0.5
            button.layer.shadowRadius = 5
        }
    }
    
    @IBOutlet weak var myView: MyView!
    
    var replicatorLayer: CAReplicatorLayer!
    var sourceLayer: CALayer!
    
    @IBAction func buttonTapped() {
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.toValue = 1
        animation.duration = 2
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.fillMode = CAMediaTimingFillMode.both
        animation.isRemovedOnCompletion = false
        animation.delegate = self
        
        overShapeLayer.add(animation, forKey: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gradientLayer = CAGradientLayer()
        view.layer.insertSublayer(gradientLayer, at: 0)
        
        shapeLayer = CAShapeLayer()
        view.layer.addSublayer(shapeLayer)
        
        overShapeLayer = CAShapeLayer()
        view.layer.addSublayer(overShapeLayer)
        
        replicatorLayer = CAReplicatorLayer()
        sourceLayer = CALayer()
        
//        self.view.layer.addSublayer(replicatorLayer)
        myView.layer.addSublayer(replicatorLayer)
        replicatorLayer.addSublayer(sourceLayer)
    }
    
    func configShapeLayer(_ shapeLayer: CAShapeLayer) {
        shapeLayer.frame = view.bounds
        let path = UIBezierPath()
        path.move(to: CGPoint(x: self.view.frame.width/2-100, y: self.view.frame.height/2-shapeLayer.lineWidth))
        path.addLine(to: CGPoint(x: self.view.frame.width/2+100, y: self.view.frame.height/2-shapeLayer.lineWidth))
        shapeLayer.path = path.cgPath
    }
    
    override func viewDidLayoutSubviews() {
        gradientLayer.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height/2)
        
        configShapeLayer(shapeLayer)
        configShapeLayer(overShapeLayer)
        
        startAnimation(delay: 0.1, replicates: 30)
    }
    
    override func viewWillLayoutSubviews() {
//        replicatorLayer.frame = self.view.bounds
//        replicatorLayer.position = self.view.center
        replicatorLayer.frame = myView.frame
        replicatorLayer.position = CGPoint(x: myView.frame.width/2, y: myView.frame.height/2)
        
        sourceLayer.frame = CGRect(x: 0, y: 0, width: 5, height: 17)
        sourceLayer.backgroundColor = UIColor.black.cgColor
        sourceLayer.position = CGPoint(x: myView.frame.width/2, y: myView.frame.height/2)
        sourceLayer.anchorPoint = CGPoint(x: 0, y: 6)
    }
    
    func startAnimation(delay: TimeInterval, replicates: Int) {
        replicatorLayer.instanceCount = replicates
        let angle = CGFloat(2*Double.pi)/CGFloat(replicates)
        replicatorLayer.instanceTransform = CATransform3DMakeRotation(angle, 0, 0, 1)
        replicatorLayer.instanceDelay = delay
        
        sourceLayer.opacity = 0
        
        let opacityAnimation = CABasicAnimation(keyPath: "opacity")
        opacityAnimation.fromValue = 1.0
        opacityAnimation.toValue = 0.0
        opacityAnimation.duration = Double(replicates)*delay
        opacityAnimation.repeatCount = Float.infinity
        
        sourceLayer.add(opacityAnimation, forKey: nil)
    }

}

extension ViewController: CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        performSegue(withIdentifier: "showSecondVC", sender: self)
    }
}
