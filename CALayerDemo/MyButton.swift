//
//  MyButton.swift
//  CALayerDemo
//
//  Created by Admin on 12.02.2020.
//  Copyright © 2020 Denis Silych. All rights reserved.
//

import UIKit

@IBDesignable

class MyButton: UIButton {
    
    private var radius = CGSize()
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            radius = CGSize(width: cornerRadius, height: cornerRadius)
        }
    }
    @IBInspectable var color: UIColor = .green
    
    var path: UIBezierPath?
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topRight, .bottomLeft], cornerRadii: radius)
        color.setFill()
        path?.fill()
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        if let path = path {
            return path.contains(point)
        }
        return false
    }
}
