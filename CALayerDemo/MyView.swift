//
//  MyView.swift
//  CALayerDemo
//
//  Created by Admin on 12.02.2020.
//  Copyright © 2020 Denis Silych. All rights reserved.
//

import UIKit

@IBDesignable

class MyView: UIView {
    
    private var size = CGSize()
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            size = CGSize(width: cornerRadius, height: cornerRadius)
        }
    }

    override func layoutSubviews() {
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .bottomRight], cornerRadii: size).cgPath
        layer.mask = shapeLayer
    }
    
    override func prepareForInterfaceBuilder() {
        setNeedsLayout()
    }

}
