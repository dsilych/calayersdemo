//
//  SecondViewController.swift
//  CALayerDemo
//
//  Created by Admin on 12.02.2020.
//  Copyright © 2020 Denis Silych. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    var shapeLayer: CAShapeLayer! {
        didSet {
            shapeLayer.lineWidth = 20
            shapeLayer.lineCap = .round
            shapeLayer.fillColor = nil
            shapeLayer.strokeEnd = 1
            shapeLayer.strokeColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1).cgColor
        }
    }
    var overShapeLayer: CAShapeLayer! {
        didSet {
            overShapeLayer.lineWidth = 20
            overShapeLayer.lineCap = .round
            overShapeLayer.fillColor = nil
            overShapeLayer.strokeEnd = 0
            overShapeLayer.strokeColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).cgColor
        }
    }
    
    @IBOutlet weak var image: UIImageView! {
        didSet {
            image.layer.cornerRadius = image.frame.size.height/2
            image.layer.masksToBounds = true
            let borderColor = UIColor.white
            image.layer.borderColor = borderColor.cgColor
            image.layer.borderWidth = 10
        }
    }
    @IBOutlet weak var button: UIButton! {
        didSet {
            button.layer.shadowOffset = CGSize(width: 0, height: 5)
            button.layer.shadowOpacity = 0.5
            button.layer.shadowRadius = 5
        }
    }
    
    @IBOutlet weak var label: UILabel!
    @IBAction func takeCupTaped() {
        label.isHidden = false
        startAnimation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        shapeLayer = CAShapeLayer()
        view.layer.addSublayer(shapeLayer)
        
        overShapeLayer = CAShapeLayer()
        view.layer.addSublayer(overShapeLayer)
        
        print("Position in superclass view: \(image.frame), Local position: \(image.bounds)")
    }
    
    override func viewDidLayoutSubviews() {
        configShapeLayer(shapeLayer)
        configShapeLayer(overShapeLayer)
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.toValue = 1
        animation.duration = 5
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.fillMode = CAMediaTimingFillMode.both
        animation.isRemovedOnCompletion = false
        
        overShapeLayer.add(animation, forKey: nil)
    }
    
    func configShapeLayer(_ shapeLayer: CAShapeLayer) {
        shapeLayer.frame = view.bounds
        let path = UIBezierPath(arcCenter: CGPoint(x: image.frame.origin.x+image.frame.width/2, y: image.frame.origin.y+image.frame.height/2), radius: image.frame.height/2, startAngle: 0, endAngle: CGFloat(2*Double.pi), clockwise: false)
        shapeLayer.path = path.cgPath
    }
    
    func startAnimation() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.fromValue = label.layer.position
        animation.toValue = [label.layer.position.x, label.layer.position.y+40]
        animation.duration = 1
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        
        label.layer.add(animation, forKey: "labelUp")
        
//        animation2.repeatCount = Float.infinity

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
